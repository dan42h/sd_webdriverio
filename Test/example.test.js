import SuperDry from '../pages/Superdry.js'
import { AssertionError } from 'assert';

// Useful links:
// WebdriverIO API - https://webdriver.io/docs/api.html
// Chai assertions - https://www.chaijs.com/api/bdd/

// To run input in CMD line: grunt --spec=example.test --env=https://www.superdry.com

describe('Superdry: general automation javascript test', () => {
    it('Opens mens Tshirts', () => {
        //browser.maximizeWindow();
        browser.url('mens/t-shirts');

    });
    it('clicks a random product', () => {
        let max = SuperDry.productImages.length;
        let min = 1;
        let randomSelection = Math.floor((Math.random() * (max - min)) + min);
        SuperDry.productImages[randomSelection].click();
    });

    // Challenge 1 - count the words in the product description
    it('counts the words in the product description', () => {
        console.log(`Number of words in description: ${browser.countWords(SuperDry.description[1])}`);
        
        // Not currently outputting count of description, just whole description. Need to rework...
    });

    // Challenge 2 - Confirm that the hex color code is as expected
    it('confirm colour of free delivery link', () => {
        assert.include(SuperDry.free_delivery.getCSSProperty('color').parsed.hex,'#ff5e00');

        // Currently outputting color, Need to add assertion to confirm color hex value is #ff5e00...
    });

    // Challenge 3 - write to the console a list of the available sizes
    it('outputs available sizes', () => {
        browser.getAvailableSizes(SuperDry.sizesListBoxes);
    });

});
