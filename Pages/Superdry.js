import Page from './Page'

class Superdry extends Page {
  
  get productImages()  {
      return $$('.photo')
  }
  get description()  {
    return $("div.description-container").$$("p");
  }
  get free_delivery()  {
    return $(".free-delivery")
  }
  get sizesListBoxes(){
  return $("div.size-box-container").$$("div.size-box")
  }
}

export default new Superdry()
