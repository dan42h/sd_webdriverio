# Superdry Javascript Automation Assessment

**Get started**

1. Install nodeJS: https://nodejs.org/en/
2. Extract the project to a folder
3. From this folder install your dependencies

```
npm install
```
 
**Run Tests**

```
grunt --spec=example.test --env=https://www.superdry.com
```

This runs the spec file example.test against the site superdry.com

The script should pass... but its not doing what we expected.

**Task**

Edit the test to make it function as expected. Some code comments have been added to support you. If you are not familiar with Javascript as a language I would suggest leaning into the useful links section for support.

**Useful links:**

- WebdriverIO API - https://webdriver.io/docs/api.html
- Chai assertions - https://www.chaijs.com/api/bdd/
- Javascript basics - https://www.w3schools.com/jsref/
