module.exports = function(grunt) {
    grunt.config("environment", (function() {
        return grunt.option("env");
    })());
    grunt.config("spec", (function() {
        let spec = "";
        let path = "";
        if (grunt.option("spec") !== undefined) {
            spec = grunt.option("spec");
            path = ["./Test/**/" + spec + ".js"];
        } else if (grunt.option("folder") !== undefined) {
            folder = grunt.option("folder");
            path = ["./Test/**/" + folder + "/**/*.js"];
        } else {
            path = "./Test/**/*.js";
        }
        return path;
    })());
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        webdriver: {
            test: {
                configFile: "./config/wdio.conf.local.js",
                    specs: grunt.config("spec"),
                    baseUrl: grunt.config("environment"),
                    capabilities: [{"browserName": "chrome"}],
            },
        },
    });

    grunt.loadNpmTasks("grunt-mocha");
    grunt.loadNpmTasks("grunt-webdriver");
    grunt.registerTask("default", ["webdriver:test"]);
    grunt.registerTask("local", ["webdriver:test"]);
};
