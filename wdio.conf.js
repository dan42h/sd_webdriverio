let timeout = 48 * 10000;

exports.config = {
    reporters: [
        'spec',
        ['junit', {
            outputDir: './reports/',
            outputFileFormat: function() {
                return `testresult.xml`
            }
        }]
    ],
    updateJob: false,
    exclude: [],
    suites: {},
    coloredLogs: true,
    screenshotPath: "./reports/ErrorShots",
    waitforTimeout: 30000,
    deprecationWarnings: false,
    plugins: {
        "wdio-screenshot": {}
    },
    framework: "mocha",
    mochaOpts: {
        ui: "bdd",
        compilers: ["js:babel-register"],
        timeout: timeout
    },

    // Gets executed before all workers get launched.
    onPrepare() {
    },
    // Gets executed before test execution begins. At this point you will have access to all global
    // variables like `browser`. It is the perfect place to define custom commands.
    before(capabilities, specs) {
        // Chai section
        const chai = require("chai");
        global.expect = chai.expect;
        global.assert = chai.assert;
        chai.Should();

        // determine form factor
        let formFactor = 'desktop';
        if (('true' === capabilities["real_mobile"]) || ('false' === capabilities['real_mobile'])) {
            formFactor = 'mobile';
        }
        global.formFactor = formFactor;
        if (formFactor === "desktop") {
            browser.maximizeWindow();
           // browser.setWindowSize(1920, 1080)
        }
        let platform = capabilities.browserName;
        if (platform === undefined) {
            platform = capabilities.device;
        }
        global.platform = platform;
        //console.dir(capabilities, {depth: null, colors: true});
        //console.dir(specs, {depth: null, colors: true});
        browser.addCommand("countWords", function (words) {
            return words.getText().split(' ').length
        });
        browser.addCommand("getAvailableSizes", function(sizes){
            let result = sizes.map((result) => {
                if(!result.getAttribute('class').includes('invalid'))
                {
                return result.getAttribute('data-size');
                }
            });        
            console.log('Available Sizes: '+result);
        });
    },
    afterTest() {
    },
    after() {
    },
    // Gets executed after all workers got shut down and the process is about to exit. It is not
    // possible to defer the end of the process using a promise.
    onComplete() {
    }
};
